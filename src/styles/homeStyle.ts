import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {PrimaryTheme} from '../styles/Themes';

export const buttonStyle:any={
    btnContainerStyle:{
        height:50,
        width:wp(80),
        backgroundColor:PrimaryTheme.$PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5
    },
    btnTextStyle:{
        color:'#fff'
    },
    additionalStyle:{
        marginVertical:10
    }
}

export const homeStyle:any={
    screenContainer:{
        flex:1,
        alignItems: "center",
        justifyContent:"center"
    }
}