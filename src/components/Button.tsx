import * as React from 'react';
import {TouchableOpacity,View,Text} from 'react-native';
import {buttonStyle} from '../styles/homeStyle';

// interface Props{

// }
// interface State{

// }
// class Button extends React.Component<Props,State>{
//     constructor(props){
//         super(props);
//         this.state={

//         }
//     }
// }

const Button = ({ text, onPress, style }) => {
    return (
      <TouchableOpacity onPress={onPress} style={style}>
        <View style={[buttonStyle.btnContainerStyle]}>
          <Text style={[buttonStyle.btnTextStyle]}> {text} </Text>
        </View>
      </TouchableOpacity>
    )
  }

export default Button;