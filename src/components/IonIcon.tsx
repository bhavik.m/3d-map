import * as React from 'react';
import Icons from 'react-native-vector-icons/Ionicons';

import {Platform} from 'react-native';

export interface Props {
  name: any;
  color?: string;
  size?: number;
  style?: any;
  noPrefix?: boolean;
}

const IonIcon = (props: Props) => {
  const iconName = () => {
    if (Platform.OS === 'ios') {
      return 'ios-' + props.name;
    }
    return 'md-' + props.name;
  };
  return (
    <Icons
      style={props.style}
      name={props.noPrefix ? props.name : iconName()}
      color={props.color}
      size={props.size}
    />
  );
};
IonIcon.defaultProps = {
  color: 'white',
  size: 30,
  noPrefix: false,
};
export default IonIcon;
