const config = {
  MAPBOX_PUBLICKEY:
    'pk.eyJ1IjoiamFsZGhpc2FtY29tIiwiYSI6ImNrcjRyMTB2YTA5MTkyeHIyZjBwaGduMnMifQ.aU7pgN1Swfg2dnUhL8juBA',
};

export default config;
