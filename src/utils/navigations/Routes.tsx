import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../../screens/appScreens/Home';
import Map from '../../screens/appScreens/Map';

const Stack = createStackNavigator();


export enum ScreenNames {
    HOME="Home",
    MAP="Map"
}

export const appStack = () => {
    return (
        <Stack.Navigator initialRouteName={ScreenNames.HOME}>
            <Stack.Screen options={{ headerShown: false }} name={ScreenNames.HOME} component={Home} />
            <Stack.Screen options={{  }} name={ScreenNames.MAP} component={Map} />
        </Stack.Navigator>
    )
}