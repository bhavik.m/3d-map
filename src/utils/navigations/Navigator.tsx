import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {appStack} from './Routes';


interface Props{
   
}
interface State{

}

class Navigator extends React.Component<Props,State>{
    constructor(props:any){
        super(props);
        this.state={
           
        }
    }

    render(){
        return(
            <NavigationContainer>
                {appStack()}
            </NavigationContainer>
        )
    }

}

export default Navigator;