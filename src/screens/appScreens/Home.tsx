import * as React from 'react';

import {View,Text,StyleSheet,SafeAreaView,ScrollView,TouchableOpacity} from 'react-native';
import {PrimaryTheme} from '../../styles/Themes';
import Button from '../../components/Button';
import {buttonStyle,homeStyle} from '../../styles/homeStyle';
import {ScreenNames} from '../../utils/navigations/Routes';

import {path1Data} from '../../utils/pathData/path1';
import {path2Data} from '../../utils/pathData/path2';
import {path3Data} from '../../utils/pathData/path3';
import {path4Data} from '../../utils/pathData/path4';
import {path5Data} from '../../utils/pathData/path5';


interface Props{
    navigation:any;
}

interface State{
    
}

class Home extends React.Component<Props,State> {
        constructor(props){
            super(props);
            this.state={
              
            }
        }



        

        navigateToMap=(No)=>{
           // console.log(path1Data);
            const data:any = [];
            let mapData;
            if(No == 1){
                mapData = path1Data;
            }
            else if(No == 2){
                mapData = path2Data;
            }else if(No == 3){
                mapData = path3Data;
            }else if(No == 4){
                mapData = path4Data;
            }else if(No == 5){
                mapData = path5Data;
            }
            for(let i=0;i<mapData.length;i++){
               const long = (mapData[i].LONGITUDE_DEG).replace(",", ".");
               const lat = (mapData[i].LATITUDE_DEG).replace(",", ".");
               const d = [Number(long),Number(lat)];
               data.push(d);
            }
            this.props.navigation.navigate(ScreenNames.MAP,{data});
            //console.log(data);
        }
        render(){
            const {} = this.state;

            return(
                <SafeAreaView style={styles.container}>
                    <View style={homeStyle.screenContainer}>
                        <Button text={'Path 1'} onPress={()=>this.navigateToMap('1')}
                        style={buttonStyle.additionalStyle}/>
                         <Button text={'Path 2'} onPress={()=>this.navigateToMap('2')}
                        style={buttonStyle.additionalStyle}/>
                         <Button text={'Path 3'} onPress={()=>this.navigateToMap('3')}
                        style={buttonStyle.additionalStyle}/>
                         <Button text={'Path 4'} onPress={()=>this.navigateToMap('4')}
                        style={buttonStyle.additionalStyle}/>
                         <Button text={'Path 5'} onPress={()=>this.navigateToMap('5')}
                        style={buttonStyle.additionalStyle}/>
                    </View>
                </SafeAreaView>
            )
        }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
  
})

export default Home;