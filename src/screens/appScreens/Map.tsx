import MapboxGL from '@react-native-mapbox-gl/maps';
import * as React from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import IonIcon from '../../components/IonIcon';
import { PrimaryTheme } from '../../styles/Themes';
import config from '../../utils/config';

MapboxGL.setAccessToken(config.MAPBOX_PUBLICKEY);

interface Props {
  navigation: any;
  route: any;
}

interface State {
  center: any;
  initialCoords: any;
  acceptedPermations: any;
}

class Map extends React.Component<Props, State> {
  _map: any = MapboxGL;
  constructor(props) {
    super(props);
    this.state = {
      center: [],
      initialCoords: [-77.034084, 38.9],
      acceptedPermations: false,
    };
    this.onRegionDidChange = this.onRegionDidChange.bind(this);
  }

  componentDidMount = async () => {
    MapboxGL.setConnected(true);
    MapboxGL.setTelemetryEnabled(true);
  };
  async onRegionDidChange() {
    const center = await this._map.getCenter();
    this.setState({center}, () =>
      console.log('onRegionDidChange', this.state.center),
    );
  }

  renderCurrentPoint = () => {
    const {data} = this.props.route.params;
    const startPoint: any = data[0];
    const finishedPoint = data[data.length - 1];

    const route: any = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'LineString',
            coordinates: data,
          },
          style: {
            fill: 'red',
            strokeWidth: '10',
            fillOpacity: 0.6,
          },
          paint: {
            'fill-color': '#088',
            'fill-opacity': 0.8,
          },
        },
      ],
    };

    return (
      <>
        <MapboxGL.UserLocation
          renderMode="normal"
          visible={false}
          onUpdate={location => {
            const currentCoords = [
              location.coords.longitude,
              location.coords.latitude,
            ];
            // console.log(location); // current location is here
            this.setState({
              initialCoords: currentCoords,
            });
          }}
        />

        {/* current Provider location */}
        <MapboxGL.PointAnnotation
          selected={true}
          key="key1"
          id="id1"
          coordinate={startPoint}>
          <IonIcon name="location" size={30} color="#00f" />
          <MapboxGL.Callout title="Start point" />
        </MapboxGL.PointAnnotation>
        {/* user From DB location */}
        <MapboxGL.PointAnnotation
          selected={true}
          key="key2"
          id="id2"
          coordinate={finishedPoint}>
          <IonIcon name="location" size={30} color="#0f0" />
          <MapboxGL.Callout title="Finished point" />
        </MapboxGL.PointAnnotation>
        <MapboxGL.ShapeSource id="line1" shape={route}>
          <MapboxGL.LineLayer
            id="linelayer1"
            style={{
              lineColor: PrimaryTheme.$PRIMARY_COLOR,
              lineWidth: 5,
              lineCap: 'round',
            }}
          />
        </MapboxGL.ShapeSource>
        <MapboxGL.Camera
          zoomLevel={17}
          centerCoordinate={startPoint}
        />
      </>
    );
  };

  render() {
    const {} = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <View style={StyleSheet.absoluteFillObject}>
          <MapboxGL.MapView
            styleURL={MapboxGL.StyleURL.Street}
            ref={c => (this._map = c)}
            onRegionDidChange={this.onRegionDidChange}
            zoomEnabled={true}
            style={styles.maps}>
            {this.renderCurrentPoint()}
          </MapboxGL.MapView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  maps: {
    flex: 1,
  },
});

export default Map;
